# Malaria Classification CNN

Classification of blood cells into 'infected with malaria' or 'uninfected' was done using Deep Learning algorithm CNN in Python. Also, a simple web-app was created using Stremlit where users can upload the images that they wish to check for Malaria.